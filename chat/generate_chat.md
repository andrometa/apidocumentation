# Generate Chat Session
----
Create a chat session on the platform

* **URL**

  /volunteers/api/generatechat

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "token": [THE USERS TOKEN USED FOR AUTHENTICATION]
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{'channel_id': 'GFXXXXXX'}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** Invalid Token <br />
    **Content:** `{ error : "Access Denied." }`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/volunteers/api/generatechat', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: this.state.returnedToken
            })
        })
```
