# Sign Up 
----
Sign Up and Get a Token in return

* **URL**

  /auth/api/signup

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "username": [ USERS USERNAME ]
  "password": [ USERS PASSWORD ]
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"code": "Registration Succesful!", 'token': adfkerqXXXX, "userId": 15}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** Username already exists! <br />
    **Content:** `{"error": "User already exists!"}`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/auth/api/signup', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
					username: 'kompir',
					password: 'banana'                
            })
        })
```
