# Sign In 
----
Sign In and Get a Token in return

* **URL**

  /auth/api/login

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "username": [ USERS USERNAME ]
  "password": [ USERS PASSWORD ]
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"code": "success", 'token': adfkerqXXXX, "userId": 15}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** Username or Password wrong <br />
    **Content:** `{"error": "Username or Password incorrect!"}`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/auth/api/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: 'kompir',
		password: 'banana'
            })
        })
```
