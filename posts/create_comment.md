# Create Comment
----
Create a Single Comment using the platform

* **URL**

  /posts/api/comment/create

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "token": [THE USERS TOKEN USED FOR AUTHENTICATION]
	"body": [CONTENT OF THE POST]
  "post_id": [REFERENCE TO WHICH POST] 
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"code": "Success", "comment_id": 25}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** Invalid Token <br />
    **Content:** `{ error : "Access Denied." }`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/posts/api/comment/create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: this.state.returnedToken,
                post_id: 2,
                body: body,
            })
        })
```
