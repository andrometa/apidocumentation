# Get User posts
----
Get Posts by the Authenticated User

* **URL**

posts/api/post/list/user

* **Method:**

  `GET`
  
*  **URL Params**

Limit - How many posts to fetch
Token - The Users Token

* **Data Params**

N/A

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `* 

```
{"posts":[
	{
		"approved":1,
		"author_id":104,
		"body":"Nemozam da kazam NE. Bukvalno koj I da pobara od mene nesto jas go pravam toa bez razlika dali sakam ili ne zatoa sto nemozam da mu kazam ne. Sto da pravam?",
		"category": 1,
		"created":"2019-02-14T14:18:55.219312+00:00","id":120,"likes":0,
		"title":"Promenliva"
	},.........
{}]}
```


* **Sample Call:**

```
fetch('https://speakapp.mk/posts/api/post/list/user')
            .then((response) => response.json())

```
