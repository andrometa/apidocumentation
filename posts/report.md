# Report Post / Comment
----
Report a Post or Comment on the platform

* **URL**

  /posts/api/<post|comment>/report

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "token": [THE USERS TOKEN USED FOR AUTHENTICATION]
	"body": [CONTENT OF THE POST]
  "post_id": [REFERENCE TO WHICH POST] // If you are reporting Posts
  "comment_id": [REFERENCE TO WHICH COMMENT] // If you are reporting Comments
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{'code': 'Report Successful!'}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** Invalid Token <br />
    **Content:** `{ error : "Access Denied." }`

  OR

  * **Error:** 404 Not Found <br />
    **Content:** `{'error': 'Invalid Post'} or {'error': 'Invalid Comment'}`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/posts/api/post/report', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: this.state.returnedToken,
                post_id: 2,
            })
        })
```
