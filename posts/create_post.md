# Create Post
----
Create a Single Post using the platform

* **URL**

  /posts/api/post/create

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "token": [THE USERS TOKEN USED FOR AUTHENTICATION]
	"title": [TITLE OF THE POST]
	"body": [CONTENT OF THE POST]
  "category": [Number of Category] // 1 - Generalno, 2 - Vrski, 3 - Uciliste, 4 - Doma, 5 - Prijateli, 6 - Drugo
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"code": "Success", "post_id": 25}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "Access Denied." }`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/posts/api/post/create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: this.state.returnedToken,
                title: this.state.returnedUser,
                body: body,
                category: 2,
            })
        })
```
