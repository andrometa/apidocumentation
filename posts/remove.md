# Remove Post / Comment
----
Delete a Post or Comment on the platform

* **URL**

  /posts/api/<post|comment>/remove

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "token": [THE USERS TOKEN USED FOR AUTHENTICATION]
  "post_id": [REFERENCE TO WHICH POST] // If you are removing a Post
  "comment_id": [REFERENCE TO WHICH COMMENT] // If you are removing a Comment
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{'code': 'Deletion Succesful!'}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** Invalid Token <br />
    **Content:** `{ error : "Access Denied." }`

  OR

  * **Error:** 404 Not Found <br />
    **Content:** `{'error': 'Invalid Post'} or {'error': 'Invalid Comment'}`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/posts/api/post/remove', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: this.state.returnedToken,
                post_id: 2,
            })
        })
```
