# Get Comments By Post
----
Get Posts

* **URL**

posts/api/comments/list/<post_id>

* **Method:**

  `GET`
  
*  **URL Params**

Limit - How many posts to fetch


* **Data Params**

N/A

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `* 

```
{"comments": [[
	{
		"approved" : 1,
      "author_id" : 17,
      "body" : "Zoso sakas novo drustvo? Idioti se ili samo ne se 'gledate' vekje?",
		"created": "2019-03-03T00:37:49.235280+00:00",
		"id": 1963,
		"likes": 0
	},.........
{}]}
```


* **Sample Call:**

```
fetch('https://speakapp.mk/posts/api/comments/list/<post_id>')
            .then((response) => response.json())

```
