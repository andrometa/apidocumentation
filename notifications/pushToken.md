# Push a Notification Token - EXPO only ATM
----
Push an Expo Notification Token for notification handling

* **URL**

  /notifications/api/pushToken

* **Method:**

  `POST`
  
*  **URL Params**

None

* **Data Params**

```
{
  "token": [THE USERS TOKEN USED FOR AUTHENTICATION]
  "deviceId": [THE EXPO NOTIFICATION TOKEN]
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{'code': 'Success'}`
 
* **Error Response:**

  * **Error:**   Missing Field <br />
    **Content:** `{ error : "A field is missing" }`

  OR

  * **Error:** Invalid Token <br />
    **Content:** `{ error : "Access Denied." }`

* **Sample Call:**

```
fetch('https://www.speakapp.mk/notifications/api/pushToken', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                token: this.state.returnedToken
		deviceId:  ExponentPushToken[XXXXXX]
            })
        })
```
